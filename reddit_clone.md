Create a reddit clone. This will be a website in the style of reddit. How it looks is not important but it should be obvious what it is doing.

You will create a Django application with the following features:

1. Allow users to register to your website. Create a registration form (can be very simple) and a login page.
2. Users can post links to other websites. These will display on the front page
3. Users can upvote/downvote those links. Ensure the vote is only counted once
4. Users can post comments on the links. Don't worry about threading for now.
5. Users can delete their own comments.
6. The comments for the links will be hosted on a page separate from the front page.
7. Ensure actions can only be taken by logged in users with the exception of viewing the front page and reading comments.

Resources:
1. https://www.djangoproject.com/
2. Try going through the [Django Tutorial](https://docs.djangoproject.com/en/3.1/intro/tutorial01/) first. It's all you need to get a good starting foundation for Python development.