Create a discord bot. Your bot will watch a text channel in Discord and respond to certain events. You are not allowed to use the Python discord library. This will help you get familiar with JSON and REST.

*Features*
1. Join a server (aka guild) and monitor a channel.
2. Upon joining, it should print to your console the active users every 10s (or whatever) with a timestamp
3. When you see somebody type the following commands, you will respond accordingly:

| User says | Bot responds |
| --- | --- |
| !mybot echo My Message | "My Message" |
| !mybot slap @User | Gives @User a good slap |
| !mybot weather 90210 | Gives today's forecast in zip code 90210 |

Keep your bot simple and complete the objectives above. Expand it later.



*Resources*
1. The discord python library is a good place to start: https://discordpy.readthedocs.io/en/latest/index.html This will prevent you from needing to learn the whole discord api from scratch.
2. https://discordapp.com/developers/docs/intro
    a. The Discord API refers to Servers as Guilds.
    b. Think about how Discord lays out its resources. For example, Guilds have channels and users, channels have messages, etc.
3. https://realpython.com/api-integration-in-python/
4. You'll need to learn how to pull data from a REST api and display it to the user. There are many free weather APIs available. Start with https://openweathermap.org/api
